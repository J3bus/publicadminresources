@ECHO OFF

:ISADMIN
echo %USERNAME% | findstr ! > %TEMP%\admin.tmp
IF %ERRORLEVEL% EQU 0 GOTO INIT
SET /P priv=Enter the username of your privileged account:
runas /USER:AD\%PRIV% "%~dp0%~n0%~x0"
rem exit

:INIT
SET ONE=Active Directory Users and Computers
SET ONE_CMD=START /B C:\Windows\System32\dsa.msc 

SET TWO=Group Policy Management console
SET TWO_CMD=START /B C:\Windows\System32\GPMC.MSC

SET THREE=Server Manager
SET THREE_CMD=%windir%\system32\ServerManager.exe && echo.

SET FOUR=System Centre Configuration Manager
SET FOUR_CMD="%ProgramFiles(x86)%\Microsoft Configuration Manager\AdminConsole\bin\Microsoft.ConfigurationManagement.exe" 


SET FIVE=Powershell
SET FIVE_CMD=START powershell

SET SIX=Firewall
SET SIX_CMD=START /B wf.msc

SET SEVEN=SQL Server Management Studio
SET SEVEN_CMD="C:\Program Files (x86)\Microsoft SQL Server\140\Tools\Binn\ManagementStudio\Ssms.exe"

SET EIGHT=UNASSIGNED
SET EIGHT_CMD=ECHO UNASSIGNED

SET NINE=Exit
SET NINE_CMD=EXIT
GOTO MENU


:ONE
echo Starting %ONE%
%ONE_CMD%
GOTO MENU

:TWO
echo Starting %TWO%
%TWO_CMD%
GOTO MENU

:THREE
echo Starting %THREE%
%THREE_CMD%
GOTO MENU

:FOUR
echo Starting %FOUR%
%FOUR_CMD% && echo.
GOTO MENU

:FIVE
echo Starting %FIVE%
%FIVE_CMD%
GOTO MENU

:SIX
echo Starting %SIX%
%SIX_CMD%
GOTO MENU

:SEVEN
echo Starting %SEVEN%
%SEVEN_CMD%
GOTO MENU

:EIGHT
echo Starting %EIGHT%
%EIGHT_CMD%
GOTO MENU

:NINE
EXIT


:MENU
CLS
ECHO.                 
ECHO          Smith admin menu	     
ECHO.
ECHO.
ECHO        1=%ONE%
ECHO        2=%TWO%
ECHO        3=%THREE%
ECHO        4=%FOUR%
ECHO        5=%FIVE%
ECHO        6=%SIX%
ECHO        7=%SEVEN%
ECHO        8=%EIGHT%
ECHO        9=Exit To DOS
ECHO.
CHOICE /N /C:123456789 /m "What do you want?: "
IF %ERRORLEVEL% EQU 1 GOTO ONE
IF %ERRORLEVEL% EQU 2 GOTO TWO
IF %ERRORLEVEL% EQU 3 GOTO THREE
IF %ERRORLEVEL% EQU 4 GOTO FOUR
IF %ERRORLEVEL% EQU 5 GOTO FIVE
IF %ERRORLEVEL% EQU 6 GOTO SIX
IF %ERRORLEVEL% EQU 7 GOTO SEVEN
IF %ERRORLEVEL% EQU 8 GOTO EIGHT
IF %ERRORLEVEL% EQU 9 GOTO NINE
GOTO END